var Bicicleta = function(id, color, modelo){
    this.id = id;
    this.color = color;
    this.modelo = modelo
}

Bicicleta.prototype.toString = function(){
    return "Id: " + this.id + "\n  Color: " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(bici){
    this.allBicis.push(bici);
}

Bicicleta.findById = function(id){
    var bicicleta = Bicicleta.allBicis.find(x => x.id == id);
    if(bicicleta){
        return bicicleta;
    }else{
        throw new Error('No existe bicicleta con el id: ' + id);
    }
}

Bicicleta.removeById = function(id){
    Bicicleta.findById(id);
    for (let index = 0; index < Bicicleta.allBicis.length; index++) {
        if(id == Bicicleta.allBicis[index].id){
            Bicicleta.allBicis.splice(index, 1);
            break;
        }
    }
}

var a = new Bicicleta(1, "Azul", "Nuevo");
var b = new Bicicleta(2, "Rosa", "Pa los nenes");

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;